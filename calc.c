/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 *  calc.c
 *  calc is a program that will calculate prime numbers. It need some
 *  parameters to do the job
 *
 *  (C) Copyright 2013 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This file is part of calc.
 *
 *  calc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  calc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with calc.  If not, see <http://www.gnu.org/licenses/>
 */
/** @file calc.c
 * This is the main program file. We analyses options and decide what to do
 *
 */
/**
 * @author Olivier DELHOMME,
 * @version 0.0.1
 * @date 2013
 */

#include "calc.h"

static gboolean version(void);
static gboolean usage(int status);
static int manage_command_line_options(options_t *opt, int argc, char **argv);
static options_t *init_options_struct(void);
static calc_t *init_main_structure(void);
static gboolean load_calculation_parameters(options_t *opt, calc_t *main_struct);
static gboolean is_prime(a_file_t *my_file, wrapper_t *wrap, mpz_t num);

/**
 *  prints program name, version, author, date and licence
 *  to the standard output
 */
static gboolean version(void)
{
    fprintf(stdout, "\ncalc written by %s\n  %s - Version %s\n  License %s\n", CALC_AUTHORS, CALC_DATE, CALC_VERSION, CALC_LICENSE);
    return TRUE;
}


/**
 *  Function that informs the user about the command line options
 *  available with heraia
 *  @param status : integer that indicate wether to display help (!=0) or
 *                  an error message (0)
 *  @return
 *          - TRUE -> help message has been printed
 *          - FALSE -> error message has been printed
 */
static gboolean usage(int status)
{
    if (status == 0)
        {
            fprintf(stderr, "\nTry `calc --help' for more information.\n");
            fprintf(stdout, "\n\n");

            return FALSE;
        }
    else
        {
            version();

            fprintf(stdout, "\ncalc is a prime calculator.\n");
            fprintf(stdout, "\nUsage:\n  calc [options] filename\n");
            fprintf(stdout, "\noptions_t:\n");
            fprintf(stdout, "  -h, --help\t\tThis help.\n");
            fprintf(stdout, "  -v, --version\t\tProgram version information.\n");
            fprintf(stdout, "  -p, --primes_files\tFile containing primes files filenames.\n");
            fprintf(stdout, "  -c, --calc_file\tFile containing calculation parameters.\n");
            fprintf(stdout, "\n\n");

            return TRUE;
        }
}


/**
 *  Manages all the command line options and populates the
 *  options_t *opt structure accordingly
 *  @param opt (options_t *opt) filled here with the parameters found in **argv
 *  @param argc : number of command line arguments
 *  @param argv : array of string (char *) that contains arguments
 *  @return gboolean that seems to always be TRUE
 */
static int manage_command_line_options(options_t *opt, int argc, char **argv)
{
    int c = 0;

    while ((c = getopt_long(argc, argv, "vhp:c:", long_options, NULL)) != -1)
        {
            switch (c)
                {
                    case 0:                 /* long options handler                     */
                        break;

                    case 'v':
                        version();
                        opt->usage = TRUE;  /* We do not want to continue after         */
                        break;

                    case 'h':
                        usage(1);
                        opt->usage = TRUE;  /* We do not want to continue after         */
                        break;

                    case 'c':               /* Calculation parameters file              */
                        opt->param_file = g_strdup(optarg);
                        break;

                    case 'p':               /* File that contains prime files filenames */
                        opt->prime_file = g_strdup(optarg);
                        break;

                    default:
                        usage(0);
                        opt->usage = TRUE;
                }
        }

    return 0;
}


/**
 * Inits the options_t struct that contains all
 * stuff needed to managed command line options
 * within calc
 * @return returns a newly allocated options_t structure
 *         initialized to default values
 */
static options_t *init_options_struct(void)
{
    options_t *opt = NULL; /** options_t *opt is a structure to manage program's options */

    opt = (options_t *) g_malloc0(sizeof(options_t));

    opt->usage = FALSE;
    opt->param_file = NULL;
    opt->prime_file = NULL;

    return opt;
}


/**
 * Inits the main structure
 * @returns returns a newly allocated calc_t structure initialized
 *          to default values : a NULL pf_list (the empty one), begin and
 *          end initialized big numbers and the filetype to the TEXT_FILE
 *          file type.
 */
static calc_t *init_main_structure(void)
{
    calc_t *main_struct = NULL; /** calc_t *main_struct is the program's main structure */

    main_struct = (calc_t *) g_malloc0(sizeof(calc_t));

    main_struct->pf_list = NULL;  /* The empty GList !               */
    mpz_init(main_struct->begin); /* Initaliazing big number numbers */
    mpz_init(main_struct->end);
    main_struct->filetype = TEXT_FILE;

    return main_struct;
}


/**
 * This function which does not return anything loads the calculation
 * parameters from a file (written in a .ini style) and populates the
 * main structure with those parameters
 * @param opt (options_t *opt) structure which stores all options of the
 *                           program.
 * @param main_struct : structure that contains all parameters for the
 *                      calculation.
 * @todo Add, in this function (load_calculation_parameters) some tests
 *       to validate the different variables read into the configuration
 *       file.
 */
static gboolean load_calculation_parameters(options_t *opt, calc_t *main_struct)
{
    GKeyFile *file = NULL;
    gchar *key = NULL;
    int filetype = 0;
    gboolean is_ok = FALSE;

    if (opt != NULL && opt->param_file != NULL)
        {
            file = g_key_file_new();
            g_key_file_load_from_file(file, opt->param_file, G_KEY_FILE_KEEP_COMMENTS & G_KEY_FILE_KEEP_TRANSLATIONS, NULL);

            key = g_key_file_get_string(file, GN_GLOBAL_PREFS, KN_BEGIN_NUM, NULL);
            if (key != NULL)
                {
                    gmp_sscanf(key, "%Zd", main_struct->begin);
                    g_free(key);
                }

            key = g_key_file_get_string(file, GN_GLOBAL_PREFS, KN_END_NUM, NULL);
            if (key != NULL)
                {
                    gmp_sscanf(key, "%Zd", main_struct->end);
                    g_free(key);
                }

            key = g_key_file_get_string(file, GN_GLOBAL_PREFS, KN_FILE_TYPE, NULL);
            if (key != NULL)
                {
                    sscanf(key, "%d",&filetype);
                    g_free(key);
                    main_struct->filetype = (gint) filetype;
                }

            if (main_struct->filetype == TEXT_FILE ||
                main_struct->filetype == GZIP_FILE)
                {
                    is_ok = TRUE;
                }
        }

    return is_ok;
}


/**
 * Is num a prime number ?
 * @param my_file structure that contains everything to know where we are in the
 *                files that contains the prime numbers. This stucture is used
 *                to make the 'prime number iterator function' get_next_prime().
 * @param wrap is the structure that wrapps the functions (ie the functions are
 *             different if we are reading pure text files or gzip compressed
 *             files.
 * @param num is the number that we try to determine if it is a prime number or
 *        not
 * @return returns TRUE or FALSE if the number 'num' is prime or not (respectively)
 */
static gboolean is_prime(a_file_t *my_file, wrapper_t *wrap, mpz_t num)
{
    mpz_t max;
    mpz_t a_prime;
    mpz_t reminder;
    gboolean number_is_prime = TRUE;
    gchar *a_line = NULL;

    mpz_init(max);
    mpz_init(a_prime);
    mpz_init(reminder);


    mpz_sqrt(max, num);

    a_line = wrap->get_next_prime(my_file);
    gmp_sscanf(a_line, "%Zd", a_prime);
    g_free(a_line);

    while (mpz_cmp(a_prime, max) <= 0 && number_is_prime == TRUE)
        {
            mpz_cdiv_r(reminder, num, a_prime);  /* num % a_prime = reminder */

            if (mpz_cmp_ui(reminder, 0) == 0)  /* a_prime is a divisor of num */
                {
                    number_is_prime = FALSE;
                }
            else
                {
                   a_line = wrap->get_next_prime(my_file);

                   if (a_line != NULL)
                        {
                            gmp_sscanf(a_line, "%Zd", a_prime);
                            g_free(a_line);
                        }
                    else
                        {
                            number_is_prime = FALSE;
                        }
                }
        }

    mpz_clear(max);
    mpz_clear(a_prime);
    mpz_clear(reminder);

    return number_is_prime;
}


/**
 *  main program
 *  options :
 *   - --version (-v)
 *   - --help (-h)
 *   - --primes_files (-p)
 *   - --calc_file (-c)
 */
int main(int argc, char **argv)
{
    options_t *opt = NULL;            /** options_t *opt is a structure to manage the command line options                                             */
    calc_t *main_struct = NULL;     /** calc_t *main_struct is the program's main structure                                                        */
    GList *prime_file_list = NULL;  /** GList *prime_file_list is the list of all filenames of files which contains prime numbers                  */
    GList *gfile_list = NULL;       /** GList *gfile_list is the list of GFile * corresponding one to one to the files in prime_file_list          */
    a_file_t *my_file = NULL;       /** a_file_t *my_file is the structure to manage the files and the buffering system (mimics a python iterator) */
    wrapper_t *wrap = NULL;         /** wrapper_t *wrap is the wrapper that contains the right function calls upon the type of the files           */
    mpz_t i;                        /** mpz_t i is a number to iterate between the first number to test to the last one                            */
    gboolean is_ok = FALSE;         /** gboolean is_ok says if the parameters we loaded from the config file were loaded and seems ok.             */

    g_type_init();  /** We call g_type_init() as glib type initialisation is needed by gio/gio.h */
    mpz_init(i);

    main_struct = init_main_structure();
    opt = init_options_struct();

    /* Command line options evaluation */
    manage_command_line_options(opt, argc, argv);

    if (opt->usage == TRUE)
        {
            /* we already have displayed some help/info usage */
        }
    else if (opt->param_file != NULL && opt->prime_file != NULL)
        {
            is_ok = load_calculation_parameters(opt, main_struct);

            if (is_ok == TRUE)
                {
                    wrap = init_wrapper_struct(main_struct->filetype);

                    if (wrap != NULL && wrap->read_prime_file_list != NULL)
                        {
                            main_struct->pf_list = wrap->read_prime_file_list(opt);
                            /* prime_file_list = main_struct->pf_list; */

                            my_file = wrap->init_a_file_t_struct(main_struct->pf_list);
                            gfile_list = my_file->gfile_list; /* saving for future use */

                            mpz_set(i, main_struct->begin);

                            while (mpz_cmp(i, main_struct->end) <= 0)
                                {
                                    if (is_prime(my_file, wrap, i) == TRUE)
                                        {
                                            gmp_printf("%Zd\n", i);
                                        }
                                    wrap->reset_a_file_t_struct(my_file);
                                    mpz_add_ui(i, i, 2);
                                }
                        }
                    else
                        {
                            fprintf(stderr, "Filetype %d is not implemented !\n", main_struct->filetype);
                        }
                }
        }
    else
        {
            usage(0);
        }

    mpz_clear(i);

    return 0;
}
