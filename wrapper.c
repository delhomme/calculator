/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 *  wrapper.c
 *  calc is a program that will calculate prime numbers. It need some
 *  parameters to do the job
 *
 *  (C) Copyright 2013 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This file is part of calc.
 *
 *  calc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  calc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with calc.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file wrapper.c
 * Here we toggle between function calls depending on some parameters. We
 * do this to simplify the calls in the main program. The main program
 * will always call the same function (ie wrap->get_next_prime) but this
 * function will not be the same. It will depend of the type of the file
 * we are going to read.
 *
 * With this kind of structure and the way we use it we asume that the
 * prime files will not change their type while running the program.
 *
 */

#include "calc.h"

/**
 * Inits the structure of the wrapper. The calls to the functions
 * will be done directly as one might not expect the file to change
 * it's type suddenly (from TEXT to GZIP for instance).
 *
 * @param ft is an integer that indicates the type of the file (ie
 *                TEXT_FILE, GZIP_FILE...).
 */
wrapper_t *init_wrapper_struct(gint ft)
{
    wrapper_t *wrap = NULL;

    wrap = (wrapper_t *) g_malloc0(sizeof(wrapper_t));

    if (ft == TEXT_FILE)
        {
            wrap->get_next_prime = text_get_next_prime;
            wrap->read_prime_file_list = text_read_prime_file_list;
            wrap->init_a_file_t_struct = text_init_a_file_t_struct;
            wrap->reset_a_file_t_struct = text_reset_a_file_t_struct;

        }
    else
        {
            wrap->get_next_prime = NULL;
            wrap->read_prime_file_list = NULL;
            wrap->init_a_file_t_struct = NULL;
            wrap->reset_a_file_t_struct = NULL;
        }

    return wrap;
}
