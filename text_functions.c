/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 *  text_functions.c
 *  calc is a program that will calculate prime numbers. It need some
 *  parameters to do the job
 *
 *  (C) Copyright 2013 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This file is part of calc.
 *
 *  calc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  calc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with calc.  If not, see <http://www.gnu.org/licenses/>
 */
 /**
 * @file text_functions.c
 * Here we write all functions that may deal with text files.
 */

#include "calc.h"

static gchar *get_dirname(GFile *the_file);
static void read_buffer(a_file_t *my_file);

/**
 * Gets the dirname of a GFile. We assume that the GFile is a file on
 * a filesystem.
 * @param the_file : A GFile from which we want the dirname
 * @return the dirname of the GFile the_file. For instance a GFile named
 *         '../parameters.txt' may return '/home/dup/primes/' as its
 *         dirname.
 */
static gchar *get_dirname(GFile *the_file)
{
    char *pathname = NULL;
    gchar *dirname = NULL;
    gint i = 0;

    if (the_file)
        {
            pathname = g_file_get_path(the_file);

            if (pathname)
                {
                    i = strlen(pathname);

                    while (i > 0 && pathname[i] != '/')
                        {
                            i--;
                        }
                    if (i > 0)
                        {
                            dirname = g_strndup(pathname, i);
                        }
                }
        }

    return dirname;
}


/**
 * This function reads the prime file list from a file. This file contains
 * a list of files (each of them containing primes), one per line. Each
 * line is appended to the list.
 * @param opt program selected options
 * @return returns a GList of prime files to be read
 */
GList *text_read_prime_file_list(options_t *opt)
{
    GList *prime_file_list = NULL;
    GFile *the_file = NULL;
    GFileInputStream *stream = NULL;
    gsize count = TEXT_BUFFER_SIZE;
    gssize read = 1;
    gchar *a_line = NULL;
    gint begin = 0;
    gint end = 0;
    gint i = 0;
    gchar *buffer = NULL;
    gchar *dirname = NULL;
    gchar *filename = NULL;

    if (opt->prime_file)
        {
            the_file = g_file_new_for_path(opt->prime_file);

            dirname = get_dirname(the_file);

            stream = g_file_read(the_file, NULL, NULL);

            /* Reading each line of the file */
            while (read != 0 && stream != NULL)
                {
                    buffer = (gchar *) g_malloc0 (TEXT_BUFFER_SIZE + 1);
                    read = g_input_stream_read((GInputStream *) stream, buffer, count, NULL, NULL);
                    if (read != 0)
                        {
                            begin = 0;
                            end = 0;
                            for (i = 0; i < read; i++)
                                {
                                    if (buffer[i] == '\n') /* Assuming we have UNIX newlines */
                                        {
                                            end = i;
                                            a_line = g_strndup((const gchar *) buffer + begin, end - begin);
                                            filename = g_strconcat(dirname, "/", a_line, NULL);
                                            prime_file_list = g_list_append(prime_file_list, filename);
                                            g_free(a_line);
                                            begin = end + 1;
                                        }
                                }
                        }

                    if (buffer != NULL)
                        {
                            g_free(buffer);
                        }

                }

            if (dirname != NULL)
                {
                    g_free(dirname);
                }

        }

    return prime_file_list;
}


/**
 * This function inits the structure named 'a_file_t'.
 * @param prime_file_list GList that contains the names of the prime files
 * @return a newly allocated a_file_t structure with the prime_file_list
 *         in it. The list is not copied so freeing prime_file_list
 *         leads to an empty (a_file_f)->prime_file_list !
 */
a_file_t *text_init_a_file_t_struct(GList *prime_file_list)
{
    a_file_t *my_file = NULL;
    gchar *filename = NULL;
    GList *a_list = NULL;
    GFile *gfile = NULL;

    my_file = g_malloc0(sizeof(a_file_t));

    my_file->buffer = NULL;

    a_list = prime_file_list;
    while (a_list != NULL)
        {
            filename = a_list->data;
            gfile = g_file_new_for_path(filename);
            my_file->gfile_list = g_list_append(my_file->gfile_list, gfile);
            a_list = g_list_next(a_list);
        }

    if (my_file->gfile_list != NULL)
        {
            gfile = my_file->gfile_list->data;   /* First file to be loaded */
            my_file->stream = g_file_read(gfile, NULL, NULL);
        }
    else
        {
            my_file->stream = NULL;
        }

    my_file->begin = 0;
    my_file->end = 0;
    my_file->read = 0;

    return my_file;
}


/**
 * This function 'resets' the value of 'my_file' (a_file_t * structure).
 * It returns nothing as everything is done in the structure itself.
 * @param my_file : the structure we want to 'reset'.
 */
void text_reset_a_file_t_struct(a_file_t *my_file)
{
    if (my_file)
        {
            /* Closing the stream currently in use */
            g_object_unref(my_file->stream);

            if (my_file->buffer != NULL)
                {
                    g_free(my_file->buffer);
                    my_file->buffer = NULL;
                }

            my_file->gfile_list = g_list_first(my_file->gfile_list);

            if (my_file->gfile_list != NULL)
                {
                   my_file->stream = g_file_read(my_file->gfile_list->data, NULL, NULL);
                }
            else
                {
                   my_file->stream = NULL;
                }

            my_file->begin = 0;
            my_file->end = 0;
            my_file->read = 0;
        }
}


/**
 * Read a buffer from the current file and populates my_file structure
 * @param my_file the structure where we want to insert the newly read buffer
 */
static void read_buffer(a_file_t *my_file)
{
    if (my_file != NULL)
        {
            if (my_file->buffer != NULL)
                {
                    g_free(my_file->buffer);
                }

            my_file->buffer= (gchar *) g_malloc0(TEXT_BUFFER_SIZE + 1);
            my_file->read = g_input_stream_read((GInputStream *) my_file->stream, my_file->buffer, TEXT_BUFFER_SIZE, NULL, NULL);
            my_file->begin = 0;
            my_file->end = 0;
        }
}


/**
 * Returns the next prime from the current position in the prime files. It
 * mimics a python iterator.
 * @param my_file is the structure used to remember 'where' we are (within
 *        a buffer and the file list).
 * @return the next prime read from the files in the form of a string (gchar *)
 */
gchar *text_get_next_prime(a_file_t *my_file)
{
    gint i = 0;
    gchar *a_line = NULL;
    gchar *prefix = NULL;
    gchar *whole_line = NULL;

    if (my_file->buffer == NULL)
        {
            read_buffer(my_file);
        }

    while (my_file->gfile_list != NULL)
        {

            while (my_file->read > 0)
                {

                    for (i = my_file->begin; i < my_file->read; i++)
                        {
                            if (my_file->buffer[i] == '\n') /* Assuming we have UNIX newlines */
                                {
                                    my_file->end = i;
                                    a_line = g_strndup((const gchar *) my_file->buffer + my_file->begin, my_file->end - my_file->begin);
                                    my_file->begin = my_file->end + 1;

                                    if (prefix != NULL)
                                        {
                                            whole_line = g_strconcat(prefix, a_line, NULL);
                                            g_free(a_line);
                                            g_free(prefix);

                                            return whole_line;
                                        }
                                    else
                                        {
                                            return a_line;
                                        }
                                }
                        }

                    /* next buffer some line may be contained in the previous buffer */
                    my_file->end = i;
                    prefix = g_strndup((const gchar *) my_file->buffer + my_file->begin, my_file->end - my_file->begin);

                    read_buffer(my_file);

                }

            /* next file - no line can be in continued in an another file */
            /* Closing the current stream in use */
            g_object_unref(my_file->stream);

            /* Getting the next gfile. */
            my_file->gfile_list = g_list_next(my_file->gfile_list);

            if (my_file->gfile_list != NULL)
                {
                    /* Openning a stream with the next file if we have one (it should be better - else we have a serious problem)*/
                    my_file->stream = g_file_read(my_file->gfile_list->data, NULL, NULL);
                    read_buffer(my_file);
                }
        }

    return NULL;
}



