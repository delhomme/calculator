
# Quick start

To compile calc please use `./configure && make` command or if you've got
the source from the git repository the following command :
`./autoclean && ./autogen.sh && ./configure && make`

Then invoke calc with: `./calc -p prime_number_filelist.conf -c params.conf`
