/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * calc.h
 *  calc is a program that will calculate prime numbers. It need some
 *  parameters to do the job
 *
 *  (C) Copyright 2013 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This file is part of calc.
 *
 *  calc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  calc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with calc.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file calc.h
 * calc's header file.
 */

#ifndef _CALC_H_
#define _CALC_H_

#include "config.h"

#include <glib.h>
#include <gio/gio.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <gmp.h>




/**
 * @def CALC_DATE
 * defines calc's creation date
 *
 * @def CALC_AUTHORS
 * defines calc's main authors
 *
 * @def CALC_LICENSE
 * defines calc's license (at least GPL v2)
 *
 * @def CALC_VERSION
 * defines calc's current version and release date of this version
 */
#define CALC_AUTHORS "Olivier DELHOMME"
#define CALC_DATE "14 07 2013"
#define CALC_LICENSE "GPL v3 or later"
#define CALC_VERSION "0.0.1 (14 07 2013)"


/**
 * @def TEXT_FILE
 * Value for a text file.
 *
 * @def GZIP_FILE
 * Value for a gzip compressed file.
 *
 * @def NONE_FILE
 * Value for a file of which we do not know the type !
 */
#define TEXT_FILE 0
#define GZIP_FILE 1
#define NONE_FILE 256


/**
 * @struct options_t
 * Structure options_t gives a way to store program options passed from the
 * command line.
 * - gchar *prime_file is the filename of the file which contains filenames
 *                     of the prime files.
 * - gchar *param_file is the filename of the file which contains all
 *                     parameters for the calculation.
 * - gboolean usage is there to know which kind of usage message we want to
 *                  display
 */
typedef struct
{
    gchar *prime_file; /**< file containing the filenames of some prime files  */
    gchar *param_file; /**< file containing the parameters for the calculation */
    gboolean usage;    /**< to know if we displayed the usage message          */
} options_t;


/**
 * @struct calc_t
 * Structure that contains all the necessary to do the calculation.
 * - GList *pf_list is an orderer list containing gchar *filenames.
 *                  Each of those files is containing the prime
 *                  numbers (first file begins with 2,3,5...).
 * - mpz_t begin is the first number that we want to test its primality.
 * - mpz_t end is the last number to be tested.
 * - gint filetype is the filetype according to the constants TEXT_FILE, GZIP_FILE
 *                 an so on...
 */
typedef struct
{
    GList *pf_list;  /**< List of filenames containing the prime numbers        */
    mpz_t begin;     /**< Where to begin                                        */
    mpz_t end;       /**< End the calculation at that end !                     */
    gint filetype;   /**< File type according to TEXT_FILE, GZIP_FILE constants */
} calc_t;


/**
 * @struct a_file_t
 * Structure to remember where we are within the file list (a list of GFile
 * actually) and within a single buffer. This structure helps to mimic a
 * python iterator in the function get_next_prime.
 */
typedef struct
{
    GList *gfile_list;        /**< List of GFile                                  */
    GFileInputStream *stream; /**< Current read stream                            */
    gchar *buffer;            /**< Last buffer read (and current one)             */
    gint begin;               /**< First number to test                           */
    gint end;                 /**< Last number to test                            */
    gint read;                /**< Number of bytes read when buffer was populated */
} a_file_t;


/**
 * @def GN_GLOBAL_PREFS
 * Section Name for the Global Preferences of the program (in preference file).
 *
 * @def KN_BEGIN_NUM
 * Key name for the value of the first number to test.
 *
 * @def KN_END_NUM
 * Key name for the value of the last number to test.
 *
 * @def KN_FILE_TYPE
 * Key name containing the value which says the type of the file to expect.
 * A value of TEXT_FILE stands for a text file, a value of GZIP_FILE stands
 * for a gzip compressed file.
 */
#define GN_GLOBAL_PREFS "Global Preferences"
#define KN_BEGIN_NUM "Begin number"
#define KN_END_NUM "End number"
#define KN_FILE_TYPE "File type"


/**
 * @struct option
 * Structure to give to getopt in order to parse command line options.
 */
static struct option const long_options[] =
{
    {"version", no_argument, NULL, 'v'},            /**< displays version informations                          */
    {"help", no_argument, NULL, 'h'},               /**< displays help (usage)                                  */
    {"primes_files", required_argument, NULL, 'p'}, /**< name of the file containing a list of  prime files     */
    {"calc_file", required_argument, NULL, 'c'},    /**< name of the file containing the calculation parameters */
    {NULL, 0, NULL, 0}
};


#include "text_functions.h"
#include "wrapper.h"

#endif /* _CALC_H_ */
