/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 *  wrapper.h
 *  calc is a program that will calculate prime numbers. It need some
 *  parameters to do the job
 *
 *  (C) Copyright 2013 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This file is part of calc.
 *
 *  calc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  calc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with calc.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file wrapper.h
 * wrapper's header file.
 */

#ifndef _WRAPPER_H_
#define _WRAPPER_H_

/* Function templates for the wrapper */
typedef gchar *(* PrimeFunc) (a_file_t *);     /**< get_next_prime function template        */
typedef GList *(* ReadListFunc) (options_t *);   /**< read_prime_file_list function template  */
typedef a_file_t *(* InitFileFunc) (GList *);  /**< init_a_file_t_struct function template  */
typedef void (* ResetFileFunc) (a_file_t *);   /**< reset_a_file_t_struct function template */


/**
 * @struct wrapper_t
 * This structure contains pointers to the wrapped functions.
 */
typedef struct
{
    PrimeFunc get_next_prime;
    ReadListFunc read_prime_file_list;
    InitFileFunc init_a_file_t_struct;
    ResetFileFunc reset_a_file_t_struct;
} wrapper_t;


/**
 * Inits the structure of the wrapper. The calls to the functions
 * will be done directly as one might not expect the file to change
 * it's type suddenly
 * @param ft is an integer that indicates the type of the file (ie
 *                TEXT_FILE, GZIP_FILE...).
 */
extern wrapper_t *init_wrapper_struct(gint ft);

#endif /* _WRAPPER_H_ */
