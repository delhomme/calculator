/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 *  text_functions.h
 *  calc is a program that will calculate prime numbers. It need some
 *  parameters to do the job.
 *
 *  (C) Copyright 2013 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This file is part of calc.
 *
 *  calc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  calc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with calc.  If not, see <http://www.gnu.org/licenses/>
 */
 /**
 * @file text_functions.h
 * Header files for text functions that the program may use
 */

#ifndef _TEXT_FUNCTIONS_H_
#define _TEXT_FUNCTIONS_H_


/**
 * @def TEXT_BUFFER_SIZE
 * Buffer size that we want to read from the files at once (here 16 Kb).
 * We may increase this buffer when the numbers we are going to calculate
 * will become larger then 2 Kb !
 */
#define TEXT_BUFFER_SIZE 16384


extern a_file_t *text_init_a_file_t_struct(GList *prime_file_list);
extern gchar *text_get_next_prime(a_file_t *my_file);
extern GList *text_read_prime_file_list(options_t *opt);
extern void text_reset_a_file_t_struct(a_file_t *my_file);

#endif /*_TEXT_FUNCTIONS_H_ */
